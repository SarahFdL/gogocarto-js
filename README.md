GoGoCartoJs
==========
A javascript component to create terrific cartography ! GoGoCartoJs is the autonomous frontend part of the [GoGoCarto Project](https://gitlab.adullact.net/pixelhumain/GoGoCarto)

![alt text](https://gitlab.com/seballot/gogocarto-js/raw/master/docs/images/desktop.png "Desktop")
.   .   ![alt text](https://gitlab.com/seballot/gogocarto-js/raw/master/docs/images/mobile.png "Mobile")


Discover
-------------------

[Read the online full documentation](https://pixelhumain.github.io/GoGoCartoJs/)

Demo
-----

[Try it Now!](https://pixelhumain.github.io/GoGoCartoJs/web/examples)


Install / Download
--------
`npm install gogocarto-js`

Or download latest version [here](https://gitlab.com/seballot/gogocarto-js/tags)

Or use the following links
```html
<script src="https://gogocarto.fr/js/gogocarto.min.js"></script> 
<link rel="stylesheet" href="https://gogocarto.fr/assets/css/gogocarto.min.css"> 
```

Translations
--------
You are welcome to contribute translating the project using [weblate online web interface](https://hosted.weblate.org/projects/gogocarto/javascript-library/) 


Contact
--------
You can contact us at https://chat.lescommuns.org/channel/gogocarto


